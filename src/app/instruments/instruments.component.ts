import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-instruments',
  templateUrl: './instruments.component.html',
  styleUrls: ['./instruments.component.scss']
})
export class InstrumentsComponent implements OnInit {

  titreAside = 'Nos instruments';
  texteCornemuse = 'La cornemuse écossaise';
  pikCornemuse = '../assets/images/cornemuse-ecole/cornemuse-bagpipe-nhpb.jpg';
  textePercussion = 'Les percussions';
  pikPercussion = '../assets/images/cornemuse-ecole/nhpb-instrument-pipeband-percussion-cornemuserouen.jpg';

  constructor() { }

  ngOnInit() {
  }

}
