import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { NhpbComponent } from './nhpb/nhpb.component';
import { SubnavComponent } from './subnav/subnav.component';
import { FooterComponent } from './footer/footer.component';
import { AdressComponent } from './adress/adress.component';
import { MembersComponent } from './members/members.component';
import { MusicienComponent } from './musicien/musicien.component';
import { PipebandComponent } from './pipeband/pipeband.component';
import { InstrumentsComponent } from './instruments/instruments.component';
import { UniformComponent } from './uniform/uniform.component';
import { EventsComponent } from './events/events.component';
import { SchoolComponent } from './school/school.component';
import { HireusComponent } from './hireus/hireus.component';
import { MusicComponent } from './music/music.component';
import { HomeComponent } from './home/home.component';
import { AsideComponent } from './aside/aside.component';
import { TestComponent } from './test/test.component';
import { FacebookComponent } from './facebook/facebook.component';

const appRoutes: Routes = [
  { path: 'nhpb', component: NhpbComponent },
  { path: 'pipeband', component: PipebandComponent },
  { path: 'membres', component: MembersComponent },
  { path: 'instruments', component: InstrumentsComponent },
  { path: 'repertoire', component: MusicComponent },
  { path: 'uniforme', component: UniformComponent },
  { path: 'journal', component: EventsComponent },
  { path: 'ecole', component: SchoolComponent },
  { path: 'prestation', component: HireusComponent },
  { path: 'test', component: TestComponent },
  { path: 'Home', component: HomeComponent },
  { path: '',   redirectTo: '/Home', pathMatch: 'full' },
  { path: '**', redirectTo: '/not-found' },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    NhpbComponent,
    SubnavComponent,
    FooterComponent,
    AdressComponent,
    MembersComponent,
    MusicienComponent,
    PipebandComponent,
    InstrumentsComponent,
    UniformComponent,
    EventsComponent,
    SchoolComponent,
    HireusComponent,
    MusicComponent,
    HomeComponent,
    AsideComponent,
    TestComponent,
    FacebookComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    AngularFontAwesomeModule,
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
