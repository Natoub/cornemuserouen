import { Component, NgModule, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  titreUn = 'Test';

  constructor() { }

  ngOnInit() {
  }

}
