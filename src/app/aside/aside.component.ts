import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})
export class AsideComponent implements OnInit {

  @Input() asideTitle: string;
  @Input() asideComment: string;
  @Input() asidePik: string;
  @Input() asideComment2: string;
  @Input() asidePik2: string;
  @Input() lement: string;
  // Test de liste. Cette liste fonctionne mais elle est la m^me sur tout les asides
  /* elements = [
    {
      name : 'Highlands Games de Luzarches',
      reference : 'repertoire'
    },
    'Libération de Perriers-Sur-Andelle',
  'Armada 2019', 'Pentecôte en kilt', 'United Pipers for Peace', 'Concours de Lorient'];
  */

  constructor() { }

  ngOnInit() {
  }

}
