import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-musicien',
  templateUrl: './musicien.component.html',
  styleUrls: ['./musicien.component.scss']
})
export class MusicienComponent implements OnInit {

  @Input() musicienName: string;
  @Input() musicienPseudo: string;

  constructor() { }

  ngOnInit() {
  }

}
