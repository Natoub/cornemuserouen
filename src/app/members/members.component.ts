import {Component, Input, NgModule, OnInit} from '@angular/core';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    FormsModule
  ]
})

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {

  LienNHPB = 'Le NHPB';
  // LienMembres = 'La composition';
  LienPresta = 'Les Prestations';
  LienUniforme = 'Les Uniformes';
  LienGrade = 'Les grades';
  LienContact = 'Contactez-nous';
  SousLienNHPB = 'nhpb';
  // SousLienMembres = 'members';
  SousLienPresta = 'nhpb/prestation';
  SousLienUniforme = 'nhpb/uniform';
  SousLienGrade = 'nhpb/grade';
  SousLienContact = 'nhpb/contact';

  // Diverres
  pseudoDiverres = 'John D'
  musicienDiverres = 'Jean Diverres';
  pikDiverres = '../assets/images/membres/diverres-john-fulldress-nhpb-cornemuserouen.jpg';

  // Lemire
  pseudoLemire = 'Al';
  musicienLemire = 'Anaël Lemire';
  pikLemire = '../assets/images/membres/lemire-anael-fulldress-nhpb-cornemuserouen.jpg';

  // Leport
  pseudoLeport = 'Mimi5';
  musicienLeport = 'Jérémy Leport';
  pikLeport = '../assets/images/membres/leport-jeremy-fulldress-nhpb-cornemuserouen.jpg';

  // Réault C
  pseudoReault = 'Cricri';
  musicienReault = 'Christophe Réault';
  pikReault = '../assets/images/membres/reault-christophe-fulldress-nhpb-cornemuserouen.jpg';

  // Réault N
  pseudoReaultN = 'Noah';
  musicienReaultN = 'Noah Réault';
  pikReaultN = '../assets/images/membres/reault-noah-fulldress-nhpb-cornemuserouen.jpg';

  // Salhorgne
  pseudoSalhorgne = 'Nico';
  musicienSalhorgne = 'Nicolas Salhorgne';
  pikSalhorgne = '../assets/images/membres/salhorgne-nicolas-fulldress-nhpb-cornemuserouen.jpg';

  // Deve
  pseudoDeve = 'Olive';
  musicienDeve = 'Olivier Dévé';
  pikDeve = '../assets/images/membres/deve-olivier-fulldress-nhpb-cornemuserouen.jpg';

  // Charbonneau
  pseudoCharbonneau = 'Pompy';
  musicienCharbonneau = 'Jean-Baptiste Charbonneau';
  pikCharbonneau = '../assets/images/membres/charbonneau-jean-baptiste-fulldress-nhpb-cornemuserouen.jpg';

  // Rivière
  pseudoRiviere = 'Francine';
  musicienRiviere = 'Francine Rivière';
  pikRiviere = '../assets/images/membres/riviere-francine-fulldress-nhpb-cornemuserouen.jpg';

  // Lamarre
  pseudoLamarre = 'Picsou';
  musicienLamarre = 'Michel Lamarre';
  pikLamarre = '../assets/images/membres/lamarre-michel-millitaire-1-nhpb-cornemuserouen.jpg';

  // Hasne
  pseudoHasne = 'Titi';
  musicienHasne = 'Thierry Hasne';
  pikHasne = '../assets/images/membres/hasne-thierry-millitaire-1-nhpb-cornemuserouen.jpg';

  // Sorel
  pseudoSorel = 'Gérard';
  musicienSorel = 'Franck Sorel';
  pikSorel = '../assets/images/membres/sorel-franck-fulldress-nhpb-cornemuserouen.jpg';

  // Porcher
  pseudoPorcher = 'CP';
  musicienPorcher = 'Christophe Porcher';
  pikPorcher = '../assets/images/membres/porcher-christophe-fulldress-nhpb-cornemuserouen.jpg';

  // Quillien
  pseudoQuillien = 'Papy Blaireau';
  musicienQuillien = 'Claude Quillien';
  pikQuillien = '../assets/images/membres/quillien-claude-fulldress-nhpb-cornemuserouen.jpg';

  // Berthelot
  pseudoBerthelot = 'Jacky';
  musicienBerthelot = 'Jacques Berthelot';
  pikBerthelot = '../assets/images/membres/berthelot-jacques-civil-nhpb-cornemuserouen.jpg';

  // Boitout
  pseudoBoitout = 'OB';
  musicienBoitout = 'Olivier Boitout';
  pikBoitout = '../assets/images/membres/anonyme-princecharlie-nhpb-cornemuserouen.jpg';

  constructor() { }

  ngOnInit() {
  }

}
